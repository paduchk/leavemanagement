package org.paduchk.application;

import java.util.ArrayList;
import java.util.List;

import org.paduchk.domain.employee.EmployeeType;
import org.paduchk.domain.employee.EmployeeTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeTypeService {

	@Autowired
	EmployeeTypeRepository employeeTypeRepository;
	
	public void saveEmployeeType(EmployeeType employeeType) {
		employeeTypeRepository.save(employeeType);
	}
	
	public void deleteEmployeeType(EmployeeType employeeType) {
		employeeTypeRepository.delete(employeeType);
	}
	
	public List<String> getAllEmployeeTypeNames() {
		List<String> listOfNames = new ArrayList<>();
		for(EmployeeType empType:employeeTypeRepository.findAll()) {
			listOfNames.add(empType.getName());
		}
		return listOfNames;
	}
}
