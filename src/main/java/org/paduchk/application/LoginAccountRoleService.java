package org.paduchk.application;

import org.paduchk.domain.employee.LoginAccountRole;
import org.paduchk.domain.employee.LoginAccountRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginAccountRoleService {

	@Autowired
	LoginAccountRoleRepository loginAccountRoleRepository;
	
	void saveLoginAccountRole(LoginAccountRole loginAccountRole ) {
		loginAccountRoleRepository.save(loginAccountRole);
	}

	public LoginAccountRoleService() {
	}
}
