package org.paduchk.application;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.paduchk.application.tools.CalendarTools;
import org.paduchk.domain.calendar.DayType;
import org.paduchk.domain.calendar.Day;
import org.paduchk.domain.calendar.DayRepository;
import org.paduchk.domain.calendar.Month;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CalendarService {
	
	@Autowired
	DayRepository daysRepository;
	
	void initiateCalendar(LocalDate year) {
		Day day;
		int yearNumber = year.getYear();
		LocalDate start = LocalDate.of(yearNumber, 1, 1);
		LocalDate end = LocalDate.of(yearNumber+1, 1, 1);
		for(LocalDate date = start; date.isBefore(end); date = date.plusDays(1) ) {
			//if ( date.getDayOfWeek() != DayOfWeek.SUNDAY ) {
				day = new Day(date);
				
				daysRepository.save(day);				
			//}
		}
		return;
	}
	
	public List<Day> getAllDayInMonth(LocalDate monthDate) {
		List<Day> days = new ArrayList<>();
		int monthNo = monthDate.getMonthValue();
		int yearNo = monthDate.getYear();
		int lengthOfMonth = LocalDate.of(yearNo,monthNo, 01).lengthOfMonth();
		days = daysRepository.findByDateBetween(LocalDate.of(yearNo, monthNo, 01), LocalDate.of(yearNo, monthNo, lengthOfMonth) );
		return days;	
	}
		
	public List<Month> composeYearAsListOfMonts(LocalDate yearDate) {
		List<Month> months = new ArrayList<>();
		int yearNo = yearDate.getYear();
		LocalDate monthDate;
		for(int monthNo=1;monthNo <=12;monthNo++) {
			monthDate = LocalDate.of(yearNo, monthNo, 01);
			months.add(composeMonthFromDays(monthDate));
		}
		return months;
	}

	public Month composeMonthFromDays(LocalDate monthDate) {
		Month month = new Month();
		List<Day> days = getAllDayInMonth(monthDate);
		month.setName(CalendarTools.monthAsString(monthDate));
		CalendarTools.setDaysTypeInMonth(days, month);
		return month;
	}	
	
	public Month composeMonthFromDays() {
		Month month = new Month();
		month.setName("Styczeń");
		month.setD1(DayType.WORKING);
		month.setD2(DayType.WORKING);
		month.setD3(DayType.WORKING);
		month.setD4(DayType.WORKING);
		month.setD5(DayType.WORKING);
		month.setD6(DayType.FREE);
		month.setD7(DayType.FREE);
		month.setD8(DayType.WORKING);
		month.setD9(DayType.WORKING);
		month.setD10(DayType.WORKING);
		month.setD11(DayType.WORKING);
		month.setD12(DayType.WORKING);
		month.setD13(DayType.WORKING);
		month.setD14(DayType.FREE);
		month.setD15(DayType.FREE);
		month.setD16(DayType.WORKING);
		month.setD17(DayType.WORKING);
		month.setD18(DayType.WORKING);
		month.setD19(DayType.WORKING);
		month.setD20(DayType.WORKING);
		month.setD21(DayType.FREE);
		month.setD22(DayType.FREE);
		month.setD23(DayType.WORKING);
		month.setD24(DayType.WORKING);
		month.setD25(DayType.WORKING);
		month.setD26(DayType.WORKING);
		month.setD27(DayType.WORKING);
		month.setD28(DayType.FREE);
		month.setD29(DayType.FREE);
		month.setD30(DayType.WORKING);
		month.setD31(DayType.EMPTY);	
		return month;
	}

	public List<Month> getAllMonthsInYear(LocalDate year) {
		List<Month> months = new ArrayList<>();
		//Month month = composeMonthFromDays();
		//months.add(month);
		months = composeYearAsListOfMonts(year);
		return months;
	}
	
}
