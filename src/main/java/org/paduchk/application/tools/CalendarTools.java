package org.paduchk.application.tools;

import java.time.LocalDate;
import java.util.List;

import org.paduchk.domain.calendar.Day;
import org.paduchk.domain.calendar.DayType;
import org.paduchk.domain.calendar.Month;

/**
 * @author kylu
 *
 */
public class CalendarTools {
	
	/**
	 * @param monthNo
	 * @return
	 */
	public static String monthAsString(LocalDate monthNo) {
		String monthString;
		switch (monthNo.getMonthValue()) {
			case 1: monthString = "Styczeń";
					break;
			case 2: monthString = "Luty";
					break;
			case 3: monthString = "Marzec";
					break;
			case 4: monthString = "Kwiecień";
					break;
			case 5: monthString = "Maj";
					break;
			case 6: monthString = "Czerwiec";
					break;
			case 7: monthString = "Lipiec";
					break;
			case 8: monthString = "Sierpień";
					break;
			case 9: monthString = "Wrzesień";
					break;
			case 10: monthString = "Październik";
					break;
			case 11: monthString = "Listopad";
					break;
			case 12: monthString = "Grudzień";
					break;
			default: monthString = "Invalid month";
					break;
		}
		return monthString;
	}
/*	
	public static Day getDayFromList(int dayNo) {
		Day day = new Day();
		return day;
	}
*/
	/**
	 * Moetoda ustawiające typy dni w obiekcie month. Ustawienie odbywa się na podstawie zawartości listy days
	 * @param days parametr wejściowy, zawiera listę dni z ustawionym typem dnia
	 * @param month obiekt modyfikowany, na którym wykonujemy operacje ustawianai typu dnia
	 */
	public static void setDaysTypeInMonth(List<Day> days,Month month) {
		LocalDate dayDate;
		for(Day day:days) {
			dayDate = day.getDate();
			switch (dayDate.getDayOfMonth()) {
			case 1: month.setD1(getDayType(day));
			break;
			case 2: month.setD2(getDayType(day));
			break;
			case 3: month.setD3(getDayType(day));
			break;
			case 4: month.setD4(getDayType(day));
			break;
			case 5: month.setD5(getDayType(day));
			break;
			case 6: month.setD6(getDayType(day));
			break;
			case 7: month.setD7(getDayType(day));
			break;
			case 8: month.setD8(getDayType(day));
			break;
			case 9: month.setD9(getDayType(day));
			break;
			case 10: month.setD10(getDayType(day));
			break;
			case 11: month.setD11(getDayType(day));
			break;
			case 12: month.setD12(getDayType(day));
			break;
			case 13: month.setD13(getDayType(day));
			break;
			case 14: month.setD14(getDayType(day));
			break;
			case 15: month.setD15(getDayType(day));
			break;
			case 16: month.setD16(getDayType(day));
			break;
			case 17: month.setD17(getDayType(day));
			break;
			case 18: month.setD18(getDayType(day));
			break;
			case 19: month.setD19(getDayType(day));
			break;
			case 20: month.setD20(getDayType(day));
			break;
			case 21: month.setD21(getDayType(day));
			break;
			case 22: month.setD22(getDayType(day));
			break;
			case 23: month.setD23(getDayType(day));
			break;
			case 24: month.setD24(getDayType(day));
			break;
			case 25: month.setD25(getDayType(day));
			break;
			case 26: month.setD26(getDayType(day));
			break;
			case 27: month.setD27(getDayType(day));
			break;
			case 28: month.setD28(getDayType(day));
			break;
			case 29: month.setD29(getDayType(day));
			break;
			case 30: month.setD30(getDayType(day));
			break;
			case 31: month.setD31(getDayType(day));
			break;
			default: break;
			}
		}
	}
	
	/**
	 * @param day
	 * @return
	 */
	private static DayType getDayType(Day day) {
		String dayType = day.getDayType();
		//int dayOfWeekNumber = day.getDate().getDayOfWeek().getValue();
		switch (dayType) {
		case "W": return DayType.FREE;
		case "P": return DayType.WORKING;	
		default: return DayType.EMPTY;
		}
	}
}
