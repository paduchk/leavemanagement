package org.paduchk.application;

import org.paduchk.domain.employee.LoginAccount;
import org.paduchk.domain.employee.LoginAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginAccountService {
	
	@Autowired
	LoginAccountRepository loginAccountRepository;
	
	void saveLoginAccount(LoginAccount loginAccount ) {
		loginAccountRepository.save(loginAccount);
	}

	LoginAccount getLoginAccount(Long id) {
		return loginAccountRepository.findOne(id);
	}
}
