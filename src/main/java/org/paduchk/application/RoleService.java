package org.paduchk.application;

import java.util.ArrayList;
import java.util.List;

import org.paduchk.domain.employee.Role;
import org.paduchk.domain.employee.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RoleService {
	
	@Autowired
	RoleRepository roleRepository;
	
	void saveRole(Role role) {
		roleRepository.save(role);
	}
	
	public List<String> getRoleNames(String filter) {
		List<String> returnList = new ArrayList<String>();
		for(Role role:roleRepository.findAll()) {
			returnList.add(role.getName());
		}
		return returnList;
	}
	
	public Role findOne(Long Id) {
		return roleRepository.findOne(Id);
	}
	
	public Role findByName(String name) {
		return roleRepository.findByNameContainingIgnoreCase(name);
	}
}
