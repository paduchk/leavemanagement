package org.paduchk.application;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;

import org.paduchk.domain.calendar.Day;
import org.paduchk.domain.employee.Employee;
import org.paduchk.domain.employee.EmployeeType;
import org.paduchk.domain.employee.LoginAccount;
import org.paduchk.domain.employee.LoginAccountRole;
import org.paduchk.domain.employee.Role;
import org.paduchk.domain.leave.ConsumedLeave;
import org.paduchk.domain.leave.DueLeave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StartupData {
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	EmployeeTypeService employeeTypeService;
	
	@Autowired
	LeaveService leaveService;
	
	@Autowired
	LoginAccountService loginAccountService;
	
	@Autowired
	LoginAccountRoleService loginAccountRoleService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	CalendarService calendarService;
	
	//LoginAc
	
	@PostConstruct
	public void init() {
		
		Employee emp1 = new Employee("Jan", "Nowak", "jan@nowak.pl", true);
		Employee emp2 = new Employee("John", "Rambo", "john@rambo.org" , true);
		Employee emp3 = new Employee("Janina", "Nowak", "john@rambo.org" , false);
		Employee emp;
		
		EmployeeType employeeType1 = new EmployeeType(1L,"Prokurator");
		EmployeeType employeeType2 = new EmployeeType(2L,"Urzędnik");
		
		LoginAccount admin1 = new LoginAccount("admin",true);
		LoginAccount user1 = new LoginAccount("user1", true);
		LoginAccount login;
		
		Role readRole = new Role("read");
		Role writeRole = new Role("write");
		Role printRole = new Role("print");
		Role deleteRole = new Role("delete");
	
		roleService.saveRole(readRole);
		roleService.saveRole(writeRole);
		roleService.saveRole(printRole);
		roleService.saveRole(deleteRole);
		
		
		loginAccountService.saveLoginAccount(admin1);
		loginAccountService.saveLoginAccount(user1);
		
		LoginAccountRole adminWrite = new LoginAccountRole(admin1,writeRole);
		LoginAccountRole user1Read = new LoginAccountRole(user1,readRole);	
		
		//admin1.getLoginAccountRole().add(adminWrite);
		//user1.getLoginAccountRole().add(user1Read);
		
		
		loginAccountRoleService.saveLoginAccountRole(adminWrite);
		loginAccountRoleService.saveLoginAccountRole(user1Read);
		
		employeeTypeService.saveEmployeeType(employeeType1);
		employeeTypeService.saveEmployeeType(employeeType2);
				
		emp1.setEmployeeType(employeeType1);
		emp2.setEmployeeType(employeeType2);
		emp3.setEmployeeType(employeeType2);
		
		emp1.setLoginAccount(admin1);
		emp2.setLoginAccount(user1);
		
		employeeService.saveEmployee(emp1);
		employeeService.saveEmployee(emp2);
		employeeService.saveEmployee(emp3);
		emp1.setEmail("jan@nowak.org");
		employeeService.saveEmployee(emp1);
		
		DueLeave emp1DueLeve = new DueLeave( 26L, LocalDate.of(2017,1,1), emp1);
		DueLeave emp2DueLeve = new DueLeave( 26L, LocalDate.of(2017,1,1), emp2);
		DueLeave emp3DueLeve = new DueLeave( 26L, LocalDate.of(2017,1,1), emp3);
		leaveService.saveDueLeave(emp1DueLeve);
		leaveService.saveDueLeave(emp2DueLeve);
		leaveService.saveDueLeave(emp3DueLeve);
		
		ConsumedLeave emp1ConsLeave1 = new ConsumedLeave(LocalDate.of(2017, 02, 01), emp1);
		ConsumedLeave emp1ConsLeave2 = new ConsumedLeave(LocalDate.of(2017, 02, 02), emp1);
		ConsumedLeave emp1ConsLeave3 = new ConsumedLeave(LocalDate.of(2017, 02, 03), emp1);
		leaveService.saveConsumedLeave(emp1ConsLeave1);
		leaveService.saveConsumedLeave(emp1ConsLeave2);
		leaveService.saveConsumedLeave(emp1ConsLeave3);
		
		emp = employeeService.getEmployee(1L);
		login = loginAccountService.getLoginAccount(1L);
		
		List<ConsumedLeave> leaves = leaveService.getEmployeeConsumedLeaves(emp1);
		calendarService.initiateCalendar(LocalDate.of(2018, 1, 1));
		//List<Days> dni = calendarService.getAllDayInMonth(2);

	System.out.println(leaves.size());
	
		
	}
}
