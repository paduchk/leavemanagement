package org.paduchk.application;

import java.util.List;

import org.paduchk.domain.employee.Employee;
import org.paduchk.domain.employee.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeService {
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	void saveEmployee(Employee employee) {
		employeeRepository.save(employee);
	}
	
	Employee getEmployee(Long employeeId) {
		return employeeRepository.findOne(employeeId);
	}
	
	List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}
	
	void setEmployeeActive(Employee employee, Boolean active) {
		employee.setActive(active);
		employeeRepository.save(employee);
	}
	
	public void deleteEmployee( Employee employee ) {
		employeeRepository.delete(employee);
	}
	
	void grantDueLeave(Employee employee) {
		return;
	}
	
	public Employee findOne(Long id) {
		return employeeRepository.findOne(id);
	}
}
