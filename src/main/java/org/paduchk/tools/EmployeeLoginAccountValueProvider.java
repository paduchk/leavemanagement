package org.paduchk.tools;

import org.paduchk.domain.employee.Employee;
import org.paduchk.domain.employee.LoginAccount;
import org.springframework.stereotype.Component;

import com.vaadin.data.ValueProvider;

@Component
public class EmployeeLoginAccountValueProvider implements ValueProvider<Employee, String> {

	LoginAccount loginAccount;
	
	public EmployeeLoginAccountValueProvider() {
	}

	@Override
	public String apply(Employee source) {
		loginAccount = source.getLoginAccount();
		if (null !=loginAccount) {
			return loginAccount.getLogin();
		} else {
			return "";
		}
	}
}
