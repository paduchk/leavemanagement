package org.paduchk.tools;

import org.paduchk.domain.employee.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import com.vaadin.data.ValueProvider;

@Component
public class EmployeeActiveValueProvider implements ValueProvider<Employee, String> {

	@Autowired
	Subtitles subtitles;
	
	public EmployeeActiveValueProvider() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String apply(Employee source) {
		return (source.getActive())?subtitles.getStringTrue():subtitles.getStringFalse();
	}
}
