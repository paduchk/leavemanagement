package org.paduchk.tools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

@Component
public class StringBooleanConverter implements Converter<String, Boolean> {

	@Autowired
	Subtitles subtitles;
	
	@Override
	public Result<Boolean> convertToModel(String value, ValueContext context) {
		// TODO Auto-generated method stub
		return Result.ok((value.equals(subtitles.getStringTrue()))?true:false);
	}

	@Override
	public String convertToPresentation(Boolean value, ValueContext context) {
		// TODO Auto-generated method stub
		return (value)?subtitles.getStringTrue():subtitles.getStringFalse();
	}

}
