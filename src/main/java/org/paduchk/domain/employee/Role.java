package org.paduchk.domain.employee;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Role {

	@Id
	@GeneratedValue
	Long id;
	String name;
	@OneToMany(mappedBy="loginAccountRoleId.roleId")
	List<LoginAccountRole> loginAccountRole;	
	
	public Role() {
	}
	
	public Role(String name) {
		this.name = name;
	}
	
}
