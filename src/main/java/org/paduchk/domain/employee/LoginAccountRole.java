package org.paduchk.domain.employee;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import org.paduchk.domain.employee.tools.LoginAccountRoleId;

import lombok.Data;

@Entity
@Data
public class LoginAccountRole {
	
	@EmbeddedId
	public LoginAccountRoleId loginAccountRoleId;

	public LoginAccountRole() {
	}

	public LoginAccountRole(LoginAccount loginAccount,Role role) {
		this.loginAccountRoleId = new LoginAccountRoleId();
		this.loginAccountRoleId.setLoginAccountId(loginAccount.getId());
		this.loginAccountRoleId.setRoleId(role.getId());
	}
}
