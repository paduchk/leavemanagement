package org.paduchk.domain.employee;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeTypeRepository extends PagingAndSortingRepository<EmployeeType, Long> {
	
	EmployeeType findByName(String name);
	
	@Override
	List<EmployeeType> findAll();

}
