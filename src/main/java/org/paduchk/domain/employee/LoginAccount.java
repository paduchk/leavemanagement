package org.paduchk.domain.employee;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class LoginAccount {

	@Id
	@GeneratedValue
	Long id;
	String login;
	Boolean active;
	@OneToMany(mappedBy="loginAccountRoleId.loginAccountId")
	List<LoginAccountRole> loginAccountRole;
	
	public LoginAccount(String login, Boolean active ) {
		this.login = login;
		this.active = active;
	}
	
	public LoginAccount() {	
	}
}
