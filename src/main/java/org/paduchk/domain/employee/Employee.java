package org.paduchk.domain.employee;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class Employee {
	
	@Id
	@GeneratedValue
	Long id;
	String firstName;
	String lastName;
	String email;
	Boolean active;
//	@OneToMany(mappedBy="employee")
//	List<DueLeave> dueLeaves;
//	@OneToMany(mappedBy="employee")
//	List<ConsumedLeave> consumedLeaves;
	@ManyToOne
	EmployeeType employeeType;
	@OneToOne
	@JoinColumn(name="LOGIN_ACCOUNT_ID")
	LoginAccount loginAccount;
	
	
	public Employee(String firstName, String lastName, String email, Boolean acitve) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.active = acitve;
	}

	public Employee() {
		super();
	}
}
