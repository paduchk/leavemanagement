package org.paduchk.domain.employee.tools;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Embeddable
@Data
public class LoginAccountRoleId implements Serializable {
	
	@NotNull
	Long loginAccountId;
	@NotNull
	Long roleId;

	public LoginAccountRoleId() {
		// TODO Auto-generated constructor stub
	}
	
	public LoginAccountRoleId(Long loginAccountId, Long roleId) {
		this.loginAccountId = loginAccountId;
		this.roleId = roleId;
	}
	
	

}
