package org.paduchk.domain.employee;

import java.util.List;

import org.paduchk.domain.employee.tools.LoginAccountRoleId;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LoginAccountRoleRepository extends PagingAndSortingRepository<LoginAccountRole, LoginAccountRoleId> {
	
	List<LoginAccountRole> findByLoginAccountRoleIdLoginAccountId(Long loginAccountId);

}
