package org.paduchk.domain.calendar;

public enum DayType {
	WORKING {
		public String toString() {
			return "P"; // P jak dzien pracyjący
		}
	}
	,FREE {
		public String toString() {
			return "W"; // w jak dzien nie pracujący - sobota , niedziela i swieta
		}			
	}
	,EMPTY {
		public String toString() {
			// not existing in callendar ex. April 31
			return "X";
		}		
	}
}
