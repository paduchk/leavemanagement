package org.paduchk.domain.calendar;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class Month {
	
	String name;
	DayType d1;
	DayType d2;
	DayType d3;
	DayType d4;
	DayType d5;
	DayType d6;
	DayType d7;
	DayType d8;
	DayType d9;
	DayType d10;
	DayType d11;
	DayType d12;
	DayType d13;
	DayType d14;
	DayType d15;
	DayType d16;
	DayType d17;
	DayType d18;
	DayType d19;
	DayType d20;
	DayType d21;
	DayType d22;
	DayType d23;
	DayType d24;
	DayType d25;
	DayType d26;
	DayType d27;
	DayType d28;
	DayType d29;
	DayType d30;
	DayType d31;
	
	public Month() {
		d29 = DayType.EMPTY;
		d30 = DayType.EMPTY;		
		d31 = DayType.EMPTY;
	}
	
	public String getD1String() {
		return d1.toString();
	}
	public String getD2String() {
		return d2.toString();
	}
	public String getD3String() {
		return d3.toString();
	}
	public String getD4String() {
		return d4.toString();
	}
	public String getD5String() {
		return d5.toString();
	}
	public String getD6String() {
		return d6.toString();
	}
	public String getD7String() {
		return d7.toString();
	}
	public String getD8String() {
		return d8.toString();
	}
	public String getD9String() {
		return d9.toString();
	}
	public String getD10String() {
		return d10.toString();
	}
	public String getD11String() {
		return d11.toString();
	}
	public String getD12String() {
		return d12.toString();
	}
	public String getD13String() {
		return d13.toString();
	}
	public String getD14String() {
		return d14.toString();
	}
	public String getD15String() {
		return d15.toString();
	}
	public String getD16String() {
		return d16.toString();
	}
	public String getD17String() {
		return d17.toString();
	}
	public String getD18String() {
		return d18.toString();
	}
	public String getD19String() {
		return d19.toString();
	}
	public String getD20String() {
		return d20.toString();
	}	
	public String getD21String() {
		return d21.toString();
	}
	public String getD22String() {
		return d22.toString();
	}
	public String getD23String() {
		return d23.toString();
	}
	public String getD24String() {
		return d24.toString();
	}
	public String getD25String() {
		return d25.toString();
	}
	public String getD26String() {
		return d26.toString();
	}
	public String getD27String() {
		return d27.toString();
	}
	public String getD28String() {
		return d28.toString();
	}
	public String getD29String() {
		return d29.toString();
	}
	public String getD30String() {
		return d30.toString();
	}
	public String getD31String() {
		return d31.toString();
	}
	
	
	
}
