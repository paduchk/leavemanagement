package org.paduchk.domain.calendar;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

public interface DayRepository extends PagingAndSortingRepository<Day, Long> {
	
	public List<Day> findByDateBetween(LocalDate start, LocalDate end);
	@Transactional
	@Modifying
	@Query("update Day d set d.dayType = ?2 where d.date = ?1")
	public void update(LocalDate date, String dayType);

}
