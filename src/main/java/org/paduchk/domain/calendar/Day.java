package org.paduchk.domain.calendar;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Day {

	@Id
	@GeneratedValue
	Long id;
	LocalDate date;
	String dayType;
	
	public Day( ) {
		super();
	}
	 
	public Day(LocalDate date) {
		super();
		this.date = date;
		int dayOfWeekNymber = date.getDayOfWeek().getValue();
		switch (dayOfWeekNymber) {
		case 6:
		case 7: this.dayType = DayType.FREE.toString();
		break;
		default: this.dayType = DayType.WORKING.toString();
		break;
		}
		//this.dayType = DayType.WORKING.toString();
	}
}
