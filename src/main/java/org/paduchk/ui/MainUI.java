package org.paduchk.ui;

import org.paduchk.ui.view.CalendarView;
import org.paduchk.ui.view.EmployeeDetailsView;
import org.paduchk.ui.view.EmployeeTypeDetailsView;
import org.paduchk.ui.view.EmployeeTypeListView;
import org.paduchk.ui.view.EmployeesListView;
import org.paduchk.ui.view.LoginAccountRoleView;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

@SpringUI
@SpringViewDisplay
@Theme("custom")
public class MainUI extends UI implements ViewDisplay {

	Navigator navigator;
	MainLayout mainLayout;
	
	public MainUI(Navigator navigator) {
		this.navigator = navigator;
	}
	
	@Override
	protected void init(VaadinRequest request) {
		mainLayout = new MainLayout();
		setContent(mainLayout);
		//navigator.navigateTo(EmployeeDetailsView.VIEW_NAME);
		navigator.navigateTo(EmployeesListView.VIEW_NAME);
		//navigator.navigateTo(EmployeeTypeListView.VIEW_NAME);
		//navigator.navigateTo(EmployeeTypeDetailsView.VIEW_NAME);
		//navigator.navigateTo(LoginAccountRoleView.VIEW_NAME);
		//navigator.navigateTo(CalendarView.VIEW_NAME);
		//navigator.navigateTo(test.VIEW_NAME);
	}

	@Override
	public void showView(View view) {
		mainLayout.setPage((Component)view);
	}
}
