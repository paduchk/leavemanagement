package org.paduchk.ui.tools;

import com.vaadin.ui.renderers.HtmlRenderer;

import elemental.json.JsonValue;

public class CalendarItemRenderer extends HtmlRenderer {

	@Override
	public JsonValue encode(String value) {
		String ret = "";
		if (value.equals("P")) {
			ret = "<div style=\"color:green\">"+value+"</div>";
		} else if (value.equals("W")) {
			ret = "<div style=\"color:red\">"+value+"</div>";
		} else 	if (value.equals("X")) {
			ret = "<div style=\"color:white\">"+value+"</div>";
		} else {
			ret = value;
		}					
		return super.encode(ret);
	}
}
