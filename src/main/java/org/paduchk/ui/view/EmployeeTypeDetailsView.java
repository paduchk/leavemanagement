package org.paduchk.ui.view;

import java.util.Arrays;

import org.paduchk.application.EmployeeService;
import org.paduchk.application.EmployeeTypeService;
import org.paduchk.domain.employee.Employee;
import org.paduchk.domain.employee.EmployeeRepository;
import org.paduchk.domain.employee.EmployeeType;
import org.paduchk.domain.employee.EmployeeTypeRepository;
import org.paduchk.tools.StringBooleanConverter;
import org.paduchk.tools.StringEmployeeTypeConverter;
import org.paduchk.tools.Subtitles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.vaadin.data.Binder;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Notification;

@SpringView(name=EmployeeTypeDetailsView.VIEW_NAME)
public class EmployeeTypeDetailsView extends EmployeeTypeDetailsViewDesign implements View {

	public static final String VIEW_NAME = "employee-type-details";
	
	Binder<EmployeeType> binder;
	
	@Autowired
	EmployeeTypeRepository employeeTypeRepository;
	
	EmployeeType employeeType;
	
	@Autowired
	public EmployeeTypeDetailsView(	Subtitles subtitles,
									EmployeeTypeService employeeTypeService	) {
		super(subtitles);
		binder = new Binder<>(EmployeeType.class);
		binder.bindInstanceFields(this);
		
		saveButton.addClickListener( e-> {
			if (binder.writeBeanIfValid(employeeType)) {
				employeeTypeRepository.save(employeeType);
				getUI().getNavigator().navigateTo(EmployeeTypeListView.VIEW_NAME);
			} else {
				Notification.show(subtitles.getFormError());
			}
			}
		);
		
		cancelButton.addClickListener( e-> {
			getUI().getNavigator().navigateTo(EmployeeTypeListView.VIEW_NAME);
		}
		);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String par = event.getParameters();
		UriComponents uc = UriComponentsBuilder.fromUriString(par).build();
		if ( null == uc.getPath() ) {
			employeeType = new EmployeeType();
		} else {
			employeeType = employeeTypeRepository.findOne(Long.parseLong(uc.getPath()));
		}	
		binder.readBean(employeeType);
	}
}
