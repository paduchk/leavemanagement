package org.paduchk.ui.view;

import org.paduchk.tools.StringBooleanConverter;
import org.paduchk.tools.StringEmployeeTypeConverter;
import org.paduchk.tools.Subtitles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;

import org.paduchk.domain.employee.Employee;
import org.paduchk.domain.employee.EmployeeRepository;
import org.paduchk.domain.employee.LoginAccount;
import org.paduchk.domain.employee.LoginAccountRepository;

import com.vaadin.data.Binder;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Notification;

@SpringView(name=LoginAccountDetailsView.VIEW_NAME)
public class LoginAccountDetailsView extends LoginAccountDetailsViewDesign implements View {

	public static final String VIEW_NAME = "login-accout-details";
	
	Binder<LoginAccount> loginAccountBinder;

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	LoginAccountRepository loginAccountRepository;
	
	Employee employee;
	LoginAccount loginAccount;	
	
	public LoginAccountDetailsView(	Subtitles subtitles,
									StringBooleanConverter booleanStringConverter) {
		super(subtitles);
		// TODO Auto-generated constructor stub
		loginAccountBinder = new Binder<>(LoginAccount.class);
		
		loginAccountBinder.forField(active)
		.withConverter(booleanStringConverter)
		.bind(LoginAccount::getActive,LoginAccount::setActive);
		active.setItems(Arrays.asList(subtitles.getStringTrue(),subtitles.getStringFalse()));
		active.setTextInputAllowed(false);
		active.setEmptySelectionAllowed(false);		
		
		
		loginAccountBinder.bindInstanceFields(this);
		
		saveButton.addClickListener( e-> {
			if ( loginAccountBinder.writeBeanIfValid(loginAccount)) {
				loginAccountRepository.save(loginAccount);
				employee.setLoginAccount(loginAccount);
				employeeRepository.save(employee);
				getUI().getNavigator().navigateTo(EmployeesListView.VIEW_NAME);				
			} else {
				Notification.show(subtitles.getFormError());
			}
		});

		cancelButton.addClickListener( e-> {
			getUI().getNavigator().navigateTo(EmployeesListView.VIEW_NAME);
		}
		);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String par = event.getParameters();
		UriComponents uc = UriComponentsBuilder.fromUriString(par).build();
		if ( null != uc.getPath()) {
			employee = employeeRepository.findOne(Long.parseLong(uc.getPath()));
			 loginAccount =  employee.getLoginAccount();
			 if ( null != loginAccount ) {
				 loginAccountBinder.readBean(loginAccount);
			 } else {
				 loginAccount = new LoginAccount();
				 loginAccount.setActive(false);
			 }
		} else {
			 loginAccount = new LoginAccount();
			 loginAccount.setActive(false);			
		}
		loginAccountBinder.readBean(loginAccount);
	}	
}
