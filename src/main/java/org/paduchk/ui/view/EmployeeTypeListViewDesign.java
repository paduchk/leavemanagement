package org.paduchk.ui.view;

import org.paduchk.domain.employee.Employee;
import org.paduchk.domain.employee.EmployeeType;
import org.paduchk.tools.Subtitles;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class EmployeeTypeListViewDesign extends VerticalLayout {
	
	Grid<EmployeeType> employeesTypeList;	
	Button addButton;
	Button editButton;
	Button removeButton;	
	
	public EmployeeTypeListViewDesign(Subtitles subtitles) {
		// TODO Auto-generated constructor stub
		Design.read(this);
		
		employeesTypeList.setColumns("name");
		
		employeesTypeList.getColumn("name").setCaption(subtitles.getEmployeeTypeCaption());
		
		addButton.setCaption(subtitles.getAddButtonCaption());
		editButton.setCaption(subtitles.getEditButtonCaption());
		removeButton.setCaption(subtitles.getRemoveButtonCaption());			
	}
}
