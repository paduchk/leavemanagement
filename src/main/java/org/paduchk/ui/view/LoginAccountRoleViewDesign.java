package org.paduchk.ui.view;

import org.paduchk.tools.Subtitles;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class LoginAccountRoleViewDesign extends VerticalLayout {

	TwinColSelect<String> roleSelect;
	Button saveButton;
	Button cancelButton;
	
	Panel loginAccountRolePanel;
	
	public LoginAccountRoleViewDesign(Subtitles subtitles) {
		Design.read(this);
		
		saveButton.setCaption(subtitles.getSaveButtonCaption());
		cancelButton.setCaption(subtitles.getCancelButtonCaption());
		loginAccountRolePanel.setCaption(subtitles.getLoginAccountRolePanelCaption());	
	}
	
}
