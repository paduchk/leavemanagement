package org.paduchk.ui.view;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.paduchk.application.EmployeeService;
import org.paduchk.application.RoleService;
import org.paduchk.domain.employee.Employee;
import org.paduchk.domain.employee.LoginAccount;
import org.paduchk.domain.employee.LoginAccountRole;
import org.paduchk.domain.employee.LoginAccountRoleRepository;
import org.paduchk.domain.employee.Role;
import org.paduchk.domain.employee.tools.LoginAccountRoleId;
import org.paduchk.tools.Subtitles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Notification;

@SpringView(name=LoginAccountRoleView.VIEW_NAME)
public class LoginAccountRoleView extends LoginAccountRoleViewDesign implements View {
	
	Employee employee;
	LoginAccount loginAccount;
	List<LoginAccountRole> loginAccountRolesList;
	Set<String> initSelection;
	Set<String> finalSelection;
	LoginAccountRole loginAccountRole;
	
	EmployeeService employeeService;
	LoginAccountRoleRepository loginAccountRoleRepository;
	RoleService roleService;

	public static final String VIEW_NAME = "login-account-role";
	
	@Autowired
	public LoginAccountRoleView(Subtitles subtitles,
								EmployeeService employeeService,
								LoginAccountRoleRepository loginAccountRoleRepository,
								RoleService roleService) {
		super(subtitles);
		this.employeeService = employeeService;
		this.loginAccountRoleRepository = loginAccountRoleRepository;
		this.roleService = roleService;
		
		cancelButton.addClickListener( e-> {
			getUI().getNavigator().navigateTo(EmployeesListView.VIEW_NAME);
		}
		);
		
		saveButton.addClickListener( e -> {
			finalSelection = roleSelect.getSelectedItems();
			if (finalSelection.equals(initSelection)) {
				Notification info =  new Notification("Nie dokonano zmian",Notification.Type.WARNING_MESSAGE);
				info.show(Page.getCurrent());
			} else {
				Notification info =  new Notification("Dokonano zmiany",Notification.Type.WARNING_MESSAGE);
				info.show(Page.getCurrent());
				// deleting existing roles
				for(LoginAccountRole loginAccountRole:loginAccountRolesList) {
					loginAccountRoleRepository.delete(loginAccountRole);
				}
				// assign login to new roles
				for (String roleName:finalSelection ) {
					Role role;
					role = roleService.findByName(roleName);
					loginAccountRole = new LoginAccountRole(loginAccount, role);
					//loginAccountRole.setLoginAccountRoleId(
					//		new LoginAccountRoleId( loginAccount.getId(), role.getId() )
					//		);
					loginAccountRoleRepository.save(loginAccountRole);
				}
			}
			getUI().getNavigator().navigateTo(EmployeesListView.VIEW_NAME);
		}
		);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String par = event.getParameters();
		Set<String>  availableRole;// = new HashSet<String>();
		UriComponents uc = UriComponentsBuilder.fromUriString(par).build();
		if ( null == uc.getPath() ) {
			Notification info =  new Notification("Wybierz pracownika",Notification.Type.WARNING_MESSAGE);
			info.show(Page.getCurrent());
		} else {
			employee = employeeService.findOne(Long.parseLong(uc.getPath()));
			loginAccount = employee.getLoginAccount();
			//getting all roles
			availableRole = new HashSet<String>(roleService.getRoleNames(null));
			roleSelect.setItems(availableRole);
			if (null != loginAccount ) {
				loginAccountRolesList =
						loginAccountRoleRepository.findByLoginAccountRoleIdLoginAccountId(loginAccount.getId());
				if ( null != loginAccountRolesList ) {
					for(LoginAccountRole selectedRole:loginAccountRolesList ) {
						Long roleId = selectedRole.getLoginAccountRoleId().getRoleId();
						roleSelect.select( roleService.findOne(roleId).getName() );
					}		
				}
				initSelection = roleSelect.getSelectedItems();
			}	
		}	
	}

}
