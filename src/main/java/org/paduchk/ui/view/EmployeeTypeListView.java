package org.paduchk.ui.view;

import org.paduchk.application.EmployeeTypeService;
import org.paduchk.domain.employee.EmployeeTypeRepository;
import org.paduchk.tools.Subtitles;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;

@SpringView(name = EmployeeTypeListView.VIEW_NAME)
public class EmployeeTypeListView extends EmployeeTypeListViewDesign implements View {
	
	public final static String VIEW_NAME = "employees-type-list";
	
	@Autowired
	EmployeeTypeRepository employeeTypeRepository;
	
	@Autowired
	public EmployeeTypeListView(Subtitles subtitles,
								EmployeeTypeService employeeTypeService) {
		super(subtitles);	
		editButton.addClickListener( e -> {
			if (employeesTypeList.getSelectedItems().iterator().hasNext()) {
				getUI().getNavigator().navigateTo(EmployeeTypeDetailsView.VIEW_NAME + "/" + employeesTypeList.getSelectedItems().iterator().next().getId());
			}
		});
		
		addButton.addClickListener( e-> {
			getUI().getNavigator().navigateTo(EmployeeTypeDetailsView.VIEW_NAME);
		});
		
		removeButton.addClickListener( e-> {
			if (employeesTypeList.getSelectedItems().iterator().hasNext()) {
				employeeTypeService.deleteEmployeeType(employeesTypeList.getSelectedItems().iterator().next());
				getUI().getNavigator().navigateTo(EmployeeTypeListView.VIEW_NAME);
			}
		});				
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		//View.super.enter(event);
		employeesTypeList.setItems(employeeTypeRepository.findAll());
	}
	
	
}
