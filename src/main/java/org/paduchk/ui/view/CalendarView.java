package org.paduchk.ui.view;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.paduchk.application.CalendarService;
import org.paduchk.domain.calendar.Day;
import org.paduchk.domain.calendar.DayRepository;
import org.paduchk.domain.calendar.DayType;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.datefield.DateTimeResolution;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid.SelectionMode;

@SpringView(name=CalendarView.VIEW_NAME)
public class CalendarView extends CalendarViewDesign implements View {

	public static final String VIEW_NAME = "calendar-view";
	
	@Autowired
	CalendarService calendarService;

	@Autowired
	DayRepository daysRepository;
	
	Day day;
	LocalDateTime startDateValue;
	LocalDateTime endDateValue;	
	
	@Override
	public void enter(ViewChangeEvent event) {
		monthListCalendar.setItems(calendarService.getAllMonthsInYear(LocalDate.of(2018, 01, 01)));
	}
	
	public CalendarView( ) {

		
		monthListCalendar.setSelectionMode(SelectionMode.NONE);
		startDate.setValue(LocalDateTime.now());
		startDate.setResolution(DateTimeResolution.DAY);
		startDate.setRequiredIndicatorVisible(true);
		startDate.setCaption("Pierwszy dzień");
		startDate.addValueChangeListener(e-> {
			startDateValue = e.getValue();
		});

		endDate.setValue(LocalDateTime.now());
		endDate.setResolution(DateTimeResolution.DAY);
		endDate.setRequiredIndicatorVisible(true);
		endDate.setCaption("Ostatni dzień");
		endDate.addValueChangeListener(e-> {
			endDateValue = e.getValue();
		});		
		
		dayType.setCaption("Typ dnia, W-wolny,P-pracujący");
		dayType.setItems(DayType.FREE.toString(),DayType.WORKING.toString());
		dayType.setSelectedItem(DayType.FREE.toString());
		dayType.setEmptySelectionAllowed(false);
		
		saveButton.setCaption("Zapisz");
		saveButton.addClickListener(e -> {
			day = new Day(startDateValue.toLocalDate());
			day.setDayType(dayType.getValue());
			//daysRepository.save(day);
			daysRepository.update(day.getDate(), dayType.getValue());
			monthListCalendar.setItems(calendarService.getAllMonthsInYear(LocalDate.of(2018, 01, 01)));
		});
		
		//saveButton.setEnabled(false);
	}
	
	
}
