package org.paduchk.ui.view;

import org.paduchk.tools.Subtitles;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

import lombok.Data;

@DesignRoot
public class LoginAccountDetailsViewDesign extends VerticalLayout {
	
	TextField login;
	ComboBox<String> active;
	Button saveButton;
	Button cancelButton;
	
	Panel loginAccountPanel;
	
	public LoginAccountDetailsViewDesign(Subtitles subtitles) {
		Design.read(this);
	
		login.setCaption(subtitles.getLoginCaption());
		active.setCaption(subtitles.getActiveCaption());
		saveButton.setCaption(subtitles.getSaveButtonCaption());
		cancelButton.setCaption(subtitles.getCancelButtonCaption());
		loginAccountPanel.setCaption(subtitles.getLoginAccountPanelCaption());				
	}
}
