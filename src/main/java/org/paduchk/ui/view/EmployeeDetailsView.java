package org.paduchk.ui.view;

import java.util.Arrays;

import org.paduchk.application.EmployeeService;
import org.paduchk.application.EmployeeTypeService;
import org.paduchk.domain.employee.Employee;
import org.paduchk.domain.employee.EmployeeRepository;
import org.paduchk.domain.employee.EmployeeTypeRepository;
import org.paduchk.domain.employee.LoginAccount;
import org.paduchk.domain.employee.LoginAccountRepository;
import org.paduchk.tools.StringBooleanConverter;
import org.paduchk.tools.StringEmployeeTypeConverter;
import org.paduchk.tools.Subtitles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.vaadin.data.Binder;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Notification;

@SpringView(name=EmployeeDetailsView.VIEW_NAME)
public class EmployeeDetailsView extends EmployeeDetailsViewDesign implements View {

	public static final String VIEW_NAME = "employee-details";
	
	Binder<Employee> employeeBinder;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	LoginAccountRepository loginAccountRepository;
	
	Employee employee;
	LoginAccount loginAccount;
	
	@Autowired
	public EmployeeDetailsView(	Subtitles subtitles,
								StringEmployeeTypeConverter stringEmployeeTypeConverter,
								StringBooleanConverter stringEmployeeActiveConverter,
								EmployeeTypeService employeeTypeService	) {
		super(subtitles);
		employeeBinder = new Binder<>(Employee.class);
		
		employeeBinder.forField(employeeType)
		.withConverter(stringEmployeeTypeConverter)
		.bind(Employee::getEmployeeType,Employee::setEmployeeType);
		employeeType.setItems(employeeTypeService.getAllEmployeeTypeNames());
		employeeType.setTextInputAllowed(false);
		employeeType.setEmptySelectionAllowed(false);
		
		employeeBinder.forField(active)
		.withConverter(stringEmployeeActiveConverter)
		.bind(Employee::getActive, Employee::setActive);
		active.setItems(Arrays.asList(subtitles.getStringTrue(),subtitles.getStringFalse()));
		active.setTextInputAllowed(false);
		active.setEmptySelectionAllowed(false);
			
		employeeBinder.bindInstanceFields(this);
		
		saveButton.addClickListener( e-> {
			if (employeeBinder.writeBeanIfValid(employee)) {
				employeeRepository.save(employee);
				getUI().getNavigator().navigateTo(EmployeesListView.VIEW_NAME);
			} else {
				Notification.show(subtitles.getFormError());
			}
			}
		);
		
		cancelButton.addClickListener( e-> {
			getUI().getNavigator().navigateTo(EmployeesListView.VIEW_NAME);
		}
		);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String par = event.getParameters();
		UriComponents uc = UriComponentsBuilder.fromUriString(par).build();
		if ( null == uc.getPath() ) {
			employee = new Employee();
			employee.setActive(false);
		} else {
			employee = employeeRepository.findOne(Long.parseLong(uc.getPath()));
		}	
		employeeBinder.readBean(employee);
	}
}
