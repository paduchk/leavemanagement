package org.paduchk.ui.view;

import org.paduchk.application.EmployeeService;
import org.paduchk.domain.employee.Employee;
import org.paduchk.domain.employee.EmployeeRepository;
import org.paduchk.tools.EmployeeActiveValueProvider;
import org.paduchk.tools.EmployeeLoginAccountValueProvider;
import org.paduchk.tools.EmployeeTypeValueProvider;
import org.paduchk.tools.Subtitles;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.ValueProvider;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;

@SpringView(name = EmployeesListView.VIEW_NAME)
public class EmployeesListView extends EmployeesListViewDesign implements View {
	
	public static final String VIEW_NAME = "employees-list";
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	public EmployeesListView(	Subtitles subtitles, 
								EmployeeTypeValueProvider employeeTypeValueProvider, 
								EmployeeActiveValueProvider employeeActiveValueProvider,
								EmployeeService employeeService,
								EmployeeLoginAccountValueProvider employeeLoginAccountValueProvider) {
		super(subtitles,employeeTypeValueProvider,employeeActiveValueProvider,employeeLoginAccountValueProvider);
		
		editButton.addClickListener( e -> {
			if (employeesList.getSelectedItems().iterator().hasNext()) {
				getUI().getNavigator().navigateTo(EmployeeDetailsView.VIEW_NAME + "/" + employeesList.getSelectedItems().iterator().next().getId());
			}
		});
		
		addButton.addClickListener( e-> {
			getUI().getNavigator().navigateTo(EmployeeDetailsView.VIEW_NAME);
		});
		
		removeButton.addClickListener( e-> {
			if (employeesList.getSelectedItems().iterator().hasNext()) {
				employeeService.deleteEmployee(employeesList.getSelectedItems().iterator().next());
				getUI().getNavigator().navigateTo(EmployeesListView.VIEW_NAME);
			}
		});
		removeButton.setEnabled(false);
		
		addUserButton.addClickListener( e-> {
			if (employeesList.getSelectedItems().iterator().hasNext()) {
				getUI().getNavigator().navigateTo(LoginAccountDetailsView.VIEW_NAME + "/" + employeesList.getSelectedItems().iterator().next().getId());
			}			
		});
		
		assignUserRoleButton.addClickListener( e -> {
			if (employeesList.getSelectedItems().iterator().hasNext()) {
				getUI().getNavigator().navigateTo(LoginAccountRoleView.VIEW_NAME + "/" + employeesList.getSelectedItems().iterator().next().getId());
			}
		});
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		employeesList.setItems(employeeRepository.findAll());
	}
}
