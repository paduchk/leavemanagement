package org.paduchk.ui.view;

import org.paduchk.domain.employee.Employee;
import org.paduchk.tools.EmployeeActiveValueProvider;
import org.paduchk.tools.EmployeeLoginAccountValueProvider;
import org.paduchk.tools.EmployeeTypeValueProvider;
import org.paduchk.tools.Subtitles;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.ValueProvider;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

import lombok.Data;

@DesignRoot
public class EmployeesListViewDesign extends VerticalLayout {
	
	Grid<Employee> employeesList;	
	Button addButton;
	Button editButton;
	Button removeButton;
	Button addUserButton;
	Button assignUserRoleButton;
	
	public EmployeesListViewDesign(	Subtitles subtitles,
									EmployeeTypeValueProvider employeeTypeValueProvider,
									EmployeeActiveValueProvider employeeActiveValueProvider,
									EmployeeLoginAccountValueProvider employeeLoginAccountValueProvider) {
		Design.read(this);
		
		employeesList.addColumn(employeeTypeValueProvider)
		.setId("employeeType").setCaption(subtitles.getEmployeeTypeCaption());

		employeesList.addColumn(employeeActiveValueProvider)
		.setId("active").setCaption(subtitles.getActiveCaption());	
		
		employeesList.addColumn(employeeLoginAccountValueProvider)
		.setId("login").setCaption(subtitles.getLoginCaption());
		
		employeesList.setColumns("firstName","lastName","email","active","employeeType","login");
		
		employeesList.getColumn("firstName").setCaption(subtitles.getFirstNameCaption());		
		employeesList.getColumn("lastName").setCaption(subtitles.getLastNameCaption());		
		employeesList.getColumn("active").setCaption(subtitles.getActiveCaption());
		employeesList.getColumn("email").setCaption(subtitles.getEmailCaption());
		
		addButton.setCaption(subtitles.getAddButtonCaption());
		editButton.setCaption(subtitles.getEditButtonCaption());
		removeButton.setCaption(subtitles.getRemoveButtonCaption());	
		addUserButton.setCaption(subtitles.getAddUserButtonCaption());
		assignUserRoleButton.setCaption(subtitles.getAssignUserRoleButtonCaption());
	}	
}
