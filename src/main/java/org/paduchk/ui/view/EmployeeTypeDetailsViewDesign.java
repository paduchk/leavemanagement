package org.paduchk.ui.view;

import org.paduchk.tools.Subtitles;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

import lombok.Data;

@DesignRoot
public class EmployeeTypeDetailsViewDesign extends VerticalLayout {
	
	TextField name;
	Button saveButton;
	Button cancelButton;
	
	public EmployeeTypeDetailsViewDesign(Subtitles subtitles) {
		Design.read(this);
	
		name.setCaption(subtitles.getEmployeeTypeCaption());
		saveButton.setCaption(subtitles.getSaveButtonCaption());
		cancelButton.setCaption(subtitles.getCancelButtonCaption());
	}
}
