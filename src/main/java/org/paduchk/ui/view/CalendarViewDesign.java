package org.paduchk.ui.view;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

import org.paduchk.domain.calendar.Month;
import org.paduchk.ui.tools.CalendarItemRenderer;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateTimeField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class CalendarViewDesign extends VerticalLayout {
	
	Grid<Month> monthListCalendar;	
	//Panel callendarPanel;
	Panel dateSelectPanel;
	DateTimeField startDate;
	DateTimeField endDate;
	NativeSelect<String> dayType;
	Button saveButton;
	//InlineDateField januar;
	Locale locale1;
	
	public CalendarViewDesign() {
		Design.read(this);

		monthListCalendar.addColumn(Month::getName, new CalendarItemRenderer()).setCaption("Miesiąc").setId("name1");
		monthListCalendar.addColumn(Month::getD1String, new CalendarItemRenderer()).setCaption("01").setId("d1");		
		monthListCalendar.addColumn(Month::getD2String, new CalendarItemRenderer()).setCaption("02").setId("d2");		
		monthListCalendar.addColumn(Month::getD3String, new CalendarItemRenderer()).setCaption("03").setId("d3");		
		monthListCalendar.addColumn(Month::getD4String, new CalendarItemRenderer()).setCaption("04").setId("d4");		
		monthListCalendar.addColumn(Month::getD5String, new CalendarItemRenderer()).setCaption("05").setId("d5");		
		monthListCalendar.addColumn(Month::getD6String, new CalendarItemRenderer()).setCaption("06").setId("d6");		
		monthListCalendar.addColumn(Month::getD7String, new CalendarItemRenderer()).setCaption("07").setId("d7");		
		monthListCalendar.addColumn(Month::getD8String, new CalendarItemRenderer()).setCaption("08").setId("d8");		
		monthListCalendar.addColumn(Month::getD9String, new CalendarItemRenderer()).setCaption("09").setId("d9");		
		monthListCalendar.addColumn(Month::getD10String, new CalendarItemRenderer()).setCaption("10").setId("d10");		
		monthListCalendar.addColumn(Month::getD11String, new CalendarItemRenderer()).setCaption("11").setId("d11");		
		monthListCalendar.addColumn(Month::getD12String, new CalendarItemRenderer()).setCaption("12").setId("d12");		
		monthListCalendar.addColumn(Month::getD13String, new CalendarItemRenderer()).setCaption("13").setId("d13");		
		monthListCalendar.addColumn(Month::getD14String, new CalendarItemRenderer()).setCaption("14").setId("d14");		
		monthListCalendar.addColumn(Month::getD15String, new CalendarItemRenderer()).setCaption("15").setId("d15");		
		monthListCalendar.addColumn(Month::getD16String, new CalendarItemRenderer()).setCaption("16").setId("d16");		
		monthListCalendar.addColumn(Month::getD17String, new CalendarItemRenderer()).setCaption("17").setId("d17");		
		monthListCalendar.addColumn(Month::getD18String, new CalendarItemRenderer()).setCaption("18").setId("d18");		
		monthListCalendar.addColumn(Month::getD19String, new CalendarItemRenderer()).setCaption("19").setId("d19");		
		monthListCalendar.addColumn(Month::getD20String, new CalendarItemRenderer()).setCaption("20").setId("d20");		
		monthListCalendar.addColumn(Month::getD21String, new CalendarItemRenderer()).setCaption("21").setId("d21");		
		monthListCalendar.addColumn(Month::getD22String, new CalendarItemRenderer()).setCaption("22").setId("d22");		
		monthListCalendar.addColumn(Month::getD23String, new CalendarItemRenderer()).setCaption("23").setId("d23");		
		monthListCalendar.addColumn(Month::getD24String, new CalendarItemRenderer()).setCaption("24").setId("d24");		
		monthListCalendar.addColumn(Month::getD25String, new CalendarItemRenderer()).setCaption("25").setId("d25");		
		monthListCalendar.addColumn(Month::getD26String, new CalendarItemRenderer()).setCaption("26").setId("d26");		
		monthListCalendar.addColumn(Month::getD27String, new CalendarItemRenderer()).setCaption("27").setId("d27");		
		monthListCalendar.addColumn(Month::getD28String, new CalendarItemRenderer()).setCaption("28").setId("d28");		
		monthListCalendar.addColumn(Month::getD29String, new CalendarItemRenderer()).setCaption("29").setId("d29");		
		monthListCalendar.addColumn(Month::getD30String, new CalendarItemRenderer()).setCaption("30").setId("d30");		
		monthListCalendar.addColumn(Month::getD31String, new CalendarItemRenderer()).setCaption("31").setId("d31");		
		
		monthListCalendar.setColumns("name1","d1","d2","d3","d4","d5","d6","d7",
				"d8","d9","d10","d11","d12","d13","d14",
				"d15","d16","d17","d18","d19","d20","d21",
				"d22","d23","d24","d25","d26","d27","d28","d29","d30","d31");
		//disabling sorting and resizing in all grid columns
		List<Column<Month, ?>> columnList =  monthListCalendar.getColumns();
		for (Column<Month, ?> column : columnList) {
			column.setSortable(false).setResizable(false);
		}
		
		//monthListCalendar.getColumn("name1").setSortable(false).setResizable(false);
		//januar.setEnabled(false);
		//januar.setValue(LocalDate.of(2018,01, 01));
		locale1 = new Locale.Builder().setLanguageTag("pl").setRegion("PL").build();
		//januar.setLocale(locale1);		
	}	
}
