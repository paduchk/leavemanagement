package org.paduchk.ui;


import org.paduchk.ui.view.CalendarView;
import org.paduchk.ui.view.EmployeeDetailsView;
import org.paduchk.ui.view.EmployeeTypeDetailsView;
import org.paduchk.ui.view.EmployeeTypeListView;
import org.paduchk.ui.view.EmployeesListView;
import org.paduchk.ui.view.LoginAccountDetailsView;
import org.paduchk.ui.view.LoginAccountRoleView;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;


public class MainLayout extends MainLayoutDesign {
	
	public void setPage(Component component ) {
		Button employees = new Button("Pracownicy");
		Button addEmployee = new Button("Dodaj pracownika ");
		Button employeesType = new Button("Typ pracownika");
		Button addEmployeesType = new Button("Dodaj typ pracownika");
		Button users = new Button("Użytkownicy");		
		Button addUser = new Button("Dodaj użytkownika");
		Button assignloginAccounrRole = new Button("przypisz rolę");
		Button calendar = new Button("Kalndarz");
		Button calendar1 = new Button("Kalndarz 1");
		

		
		content.removeAllComponents();
		menu.removeAllComponents();
		content.addComponent(component);
		menu.addComponents(employees,addEmployee,employeesType,addEmployeesType,users,addUser,calendar,calendar1);
		
		//getUI().getNavigator().navigateTo(navigationState);
		employees.addClickListener(e-> {
			getUI().getNavigator().navigateTo(EmployeesListView.VIEW_NAME);

		});
		
		addEmployee.addClickListener(e-> {
			getUI().getNavigator().navigateTo(EmployeeDetailsView.VIEW_NAME);
		});
		
		employeesType.addClickListener(e-> {
			getUI().getNavigator().navigateTo(EmployeeTypeListView.VIEW_NAME);
		});
		
		addEmployeesType.addClickListener(e-> {
			getUI().getNavigator().navigateTo(EmployeeTypeDetailsView.VIEW_NAME);
		});

		addUser.addClickListener(e-> {
			getUI().getNavigator().navigateTo(LoginAccountDetailsView.VIEW_NAME);
		});

		assignloginAccounrRole.addClickListener(e-> {
			getUI().getNavigator().navigateTo(LoginAccountRoleView.VIEW_NAME);
		});		
	
		calendar.addClickListener(e-> {
			getUI().getNavigator().navigateTo(CalendarView.VIEW_NAME);
		});
	}
}
